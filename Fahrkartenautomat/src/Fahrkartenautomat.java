﻿import java.util.Scanner;

class Fahrkartenautomat
{
	
    public static void main(String[] args)
    {
       Scanner tastatur = new Scanner(System.in);
       
    // Der fahrkartenautomat für weitere Kaufvorgänge bereitstehen
    boolean weiterKaufen = false;
       
       while(!weiterKaufen)
       {
       System.out.println("");
       System.out.println("");
       System.out.println("");
       System.out.println("Fahrkartenbestellvorgang");
       System.out.println("");
       System.out.println("======================================");
       System.out.println("");
       System.out.println("");
       
       double zuZahlenderBetrag=fahrkartenbestellungErfassen(tastatur);
        
       // Geldeinwurf
       // -----------
       double rueckgabebetrag =fahkartenBezahlen(tastatur,zuZahlenderBetrag);
       
       // Fahrscheinausgabe
       // -----------------
       fahrkartenAusgeben();
       
       // Rückgeldberechnung und -Ausgabe
       // -------------------------------
        rueckgelAusgeben(rueckgabebetrag);
       }     
       
    }
  
    

	public static double fahrkartenbestellungErfassen(Scanner scan)
    {
    	
    	System.out.println("Wählen Sie ihre Wunschfahrkarte für Berlin AB aus ");
    	System.out.printf("%50s \n","Einzelfahrschein Regeltarif AB [2,90 Eur] (1)");
    	System.out.printf("%44s \n","Tageskarte Regeltarif AB [8,60 Eur] (2)");
    	System.out.printf("%58s \n","Kleingruppen-Tageskarte Regeltarif AB [23,50 Eur] (3)");
    	System.out.println(" ");
    	System.out.println(" ");
    	System.out.print("Ihre Wahl:");
        int wahl = scan.nextInt();
        double ticketpreis=0.0;
        while(wahl!=1 && wahl !=2 && wahl!= 3)
        {
        	System.out.print("Falsche Eingabe. Wählen Sie ihre Wunschfahrkarte für Berlin AB aus");
        	System.out.print("Ihre Wahl:");
            wahl = scan.nextInt();
        	
        	
        }
        if(wahl==1)
        {
        	 ticketpreis= 2.90;
        }
        else if(wahl==2)
        {
        	ticketpreis= 8.60;
        }
        else if(wahl==3)
        {
        	ticketpreis= 23.50;
        }
        return ticketpreis;
    }
    
    public static double fahkartenBezahlen(Scanner scan, double ticketpreis)
    {
    	double eingezahlterGesamtbetrag = 0.0;
    	System.out.print("Anzahl der Tickets : ");
    	short ticketAnzahl= scan.nextShort();
        double GesamtPreis= ticketAnzahl*ticketpreis;
        
        while(eingezahlterGesamtbetrag < GesamtPreis)
        {
     	   System.out.printf("Noch zu zahlen: %.2f ", GesamtPreis -eingezahlterGesamtbetrag);
     	   System.out.print("Eingabe (mind. 5Ct, höchstens 2 Euro): ");
     	   double eingeworfeneMünze = scan.nextDouble();
            eingezahlterGesamtbetrag += eingeworfeneMünze;
        }
        double rest = eingezahlterGesamtbetrag -GesamtPreis;
        return rest;
    
    }
    
    public static void fahrkartenAusgeben()
    {
    	System.out.println("\nFahrschein wird ausgegeben");
        for (int i = 0; i < 8; i++)
        {
           System.out.print("=");
           try {
 			Thread.sleep(250);
 		} catch (InterruptedException e) {
 			// TODO Auto-generated catch block
 			e.printStackTrace();
 		}
        }
        System.out.println("\n\n");
    	
    }
    public static void rueckgelAusgeben(double rest)
    {
    	
        if(rest > 0.0)
        {
     	   System.out.printf("Der Rückgabebetrag in Höhe von %.2f EURO",rest);
     	   System.out.println(" wird in folgenden Münzen ausgezahlt:");

            while(rest >= 2.0) // 2 EURO-Münzen
            {
         	  System.out.println("2 EURO");
 	          rest-= 2.0;
            }
            while(rest >= 1.0) // 1 EURO-Münzen
            {
         	  System.out.println("1 EURO");
 	          rest -= 1.0;
            }
            while(rest >= 0.5) // 50 CENT-Münzen
            {
         	  System.out.println("50 CENT");
 	          rest -= 0.5;
            }
            while(rest >= 0.2) // 20 CENT-Münzen
            {
         	  System.out.println("20 CENT");
  	          rest -= 0.2;
            }
            while(rest >= 0.1) // 10 CENT-Münzen
            {
         	  System.out.println("10 CENT");
 	          rest -= 0.1;
            }
            while(rest >= 0.05)// 5 CENT-Münzen
            {
         	  System.out.println("5 CENT");
  	          rest -= 0.05;
            }
        }

        System.out.println("\nVergessen Sie nicht, den Fahrschein\n"+
                           "vor Fahrtantritt entwerten zu lassen!\n"+
                           "Wir wünschen Ihnen eine gute Fahrt.");
    	
    }
}