
public class Variablen 
{

	public static void main(String[] args) 
	
	{
		/* 1. Ein Zaehler soll die Programmdurchlaeufe zaehlen.
        Vereinbaren Sie eine geeignete Variable */
		int programDurchlaeufeZaehlen;

  /* 2. Weisen Sie dem Zaehler den Wert 25 zu
        und geben Sie ihn auf dem Bildschirm aus.*/
		programDurchlaeufeZaehlen = 25;
        System.out.print(programDurchlaeufeZaehlen);
        
  /* 3. Durch die Eingabe eines Buchstabens soll der Menuepunkt
        eines Programms ausgewaehlt werden.
        Vereinbaren Sie eine geeignete Variable */
        
        String buchstabe;
		

  /* 4. Weisen Sie dem Buchstaben den Wert 'C' zu
        und geben Sie ihn auf dem Bildschirm aus.*/
         buchstabe ="C";

  /* 5. Fuer eine genaue astronomische Berechnung sind grosse Zahlenwerte
        notwendig.
        Vereinbaren Sie eine geeignete Variable */
         double grosseZahnwerte;

  /* 6. Weisen Sie der Zahl den Wert der Lichtgeschwindigkeit zu
        und geben Sie sie auf dem Bildschirm aus. 299 792 458 m / s*/
         
         grosseZahnwerte= 299792458;
         System.out.print("\n\nDer Wert der Lichtgeschwindigkeit ist: " + grosseZahnwerte + " m/s." );
         

  /* 7. Sieben Personen gruenden einen Verein. Fuer eine Vereinsverwaltung
        soll die Anzahl der Mitglieder erfasst werden.
        Vereinbaren Sie eine geeignete Variable und initialisieren sie
        diese sinnvoll.*/

         int vereinMitgliederAnzahl = 2;
  /* 8. Geben Sie die Anzahl der Mitglieder auf dem Bildschirm aus.*/
         
         System.out.print("\n\nDie Anzahl der Vereinsmitglieder ist : " + vereinMitgliederAnzahl );
         

  /* 9. Fuer eine Berechnung wird die elektrische Elementarladung benoetigt.
        Vereinbaren Sie eine geeignete Variable und geben Sie sie auf
        dem Bildschirm aus.*/
         
         double elektrischeElementarLadung = 1.602176634 *Math.pow(10, -19); 
         System.out.print("\n\nDie elektrische Elementarladung ben�tigt ist : " + elektrischeElementarLadung + " C" );

  /*10. Ein Buchhaltungsprogramm soll festhalten, ob eine Zahlung erfolgt ist.
        Vereinbaren Sie eine geeignete Variable. */
         double zahlung;

  /*11. Die Zahlung ist erfolgt.
        Weisen Sie der Variable den entsprechenden Wert zu
        und geben Sie die Variable auf dem Bildschirm aus.*/
         zahlung = 25.5;
         System.out.printf("\n\nSie haben %.2f Euro bezaht. ",zahlung );
		

	}

}
