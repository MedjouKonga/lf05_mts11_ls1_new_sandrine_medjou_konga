import java.util.Scanner;
public class PCHaendler 
{

	public static void main(String[] args) 
	{
		// TODO Auto-generated method stub
		Scanner myScanner = new Scanner(System.in);
		
		// Benutzereingaben lesen
		
		String artikel=liesString(myScanner,"was moechten Sie bestellen?");
		
		int anzahl=liesInt(myScanner,"Geben Sie die Anzahl ein:");
		
        double preis =liesDouble(myScanner,"Geben Sie den Nettopreis ein:");
		
		double mwst =liesDouble(myScanner,"Geben Sie den Mehrwertsteuersatz in Prozent ein:");
		

		// Verarbeiten
		
		double nettogesamtpreis =berechneGesamtnettopreis(anzahl,preis);
		double bruttogesamtpreis =berechneGesamtbruttopreis(nettogesamtpreis,mwst);
		

		// Ausgeben
		
		rechungausgeben(artikel,anzahl,nettogesamtpreis,bruttogesamtpreis,mwst);

	}
	
	public static String liesString(Scanner scan, String text)
	{
		System.out.println(text);
		String artikel = scan.next();
	    return artikel;
		
	}
	
	public static int liesInt(Scanner scan,String text)
	{ 
		System.out.println(text);
		int anzahl = scan.nextInt();
	    return anzahl;
		
	}
	public static double liesDouble(Scanner scan,String text)
	{
		System.out.println(text);
		double preis = scan.nextDouble();
		return preis;
			
	}
	
	public static double berechneGesamtnettopreis(int anzahl, double nettopreis)
	{
		return anzahl * nettopreis ;
			
	}
	
	public static double berechneGesamtbruttopreis(double nettogesamtpreis,double mwst)
	{
		double bruttogesamtpreis = nettogesamtpreis * (1 + mwst / 100);

		   return nettogesamtpreis * (1 + mwst / 100);
			
	}
	public static void rechungausgeben(String artikel, int anzahl, double nettogesamtpreis, double bruttogesamtpreis,double mwst)
	{
		System.out.println("\tRechnung");
		System.out.printf("\t\t Netto:  %-20s %6d %10.2f %n", artikel, anzahl, nettogesamtpreis);
		System.out.printf("\t\t Brutto: %-20s %6d %10.2f (%.1f%s)%n", artikel, anzahl, bruttogesamtpreis, mwst, "%");

	}
	
   
 

}
