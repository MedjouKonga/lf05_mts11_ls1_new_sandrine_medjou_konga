import java.util.Scanner;
public class Mittelwert 
{

	public static void main(String[] args) 
	{
		// (E) "Eingabe"
	      // Werte f�r x und y festlegen:
	      // ===========================
		double x;
	    double y;
	    double m;
	    Scanner tastatur = new Scanner(System.in);
		System.out.print("Bitte geben Sie eine erste double Zahl ein : ");
		x=tastatur.nextDouble();
		
		System.out.print("Bitte geben Sie eine zweite double Zahl ein: "); 
		y= tastatur.nextDouble(); 
		
	      
	      
	      // (V) Verarbeitung
	      // Mittelwert von x und y berechnen: 
	      // ================================
	      m=berechneMittelWert( x,  y);
	      
	      // (A) Ausgabe
	      // Ergebnis auf der Konsole ausgeben:
	      // =================================
	      ausgabe(x,y,m);
	
		
	}
	
	public static double berechneMittelWert(double x, double y)
	{
		return (x + y) / 2.0;
	}
	public static void ausgabe(double x, double y, double m)
	{
		System.out.printf("Der Mittelwert von %.2f und %.2f ist %.2f\n", x, y, m);
	}

}
